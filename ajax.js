window.onload = function() {

    // ID de los Selects del HTML
    var selectPaises   = '#paises';
    var selectRegiones = '#regiones';
    var selectCiudades = '#ciudades';

    /*
     * URL de los archivos JSON cuya informacion traeremos con una llamada AJAX.
     * Como pueden ver urlRegiones y urlCiudades estan incompletas ya que les falta
     * un ID al final de la URL (la de urlRegiones es el ID del pais seleccionado
     * y la de urlCiudades es el ID de la region seleccionada).
     */
    var urlPaises   = 'http://pilote.techo.org/admin/?do=api.getPaises';
    var urlRegiones = 'http://pilote.techo.org/admin/?do=api.getRegiones?idPais=';
    var urlCiudades = 'http://pilote.techo.org/admin/?do=api.getCiudades?idRegionLT=';

    // Esta URL la utilizare para POST
    var urlRegistrarPersona = 'http://pilote.techo.org/admin/?do=api.registrarPersona';

    /*
     *  Ejecuto llamada AJAX para POST, en la cual especifico la URL,
     *  la function que sirve como CallBack, el argumento de METHOD
     *  que en este caso es POST y el ultimo corresponde al objeto que enviamos.
     */
    ajaxCall(urlRegistrarPersona, function(rta) {
        console.log(rta);
    }, 'POST', {
        nombres: 'Prueba',
        apellido: 'Prueba',
        email: 'patatasFritas@mail.com',
    });

    /*
     * Las ultimas lineas de este bloque de codigo ejecutan las llamadas de AJAX por 'GET'.
     * La primera linea trae los paises, la anteultima trae las regiones
     * apartir del pais seleccionado y la ultima trae las ciudades
     * a partir de la region seleccionada.
     */
    ajaxCall(urlPaises, functionToCall = function(data) {
        parseSelect(selectPaises, data.contenido);
    }, 'GET', null);
    runEvent(selectRegiones, selectPaises, urlRegiones);
    runEvent(selectCiudades, selectRegiones, urlCiudades);
};


/*
 * Se completa la URL con el value del selectedId
 * y la usamos para realizar la llamada AJAX
 */
function runEvent(selectId, selectedId, url) {
    document.querySelector(selectedId).onchange = function() {
        var generatedUrl = urlGenerator(selectedId, url);
        ajaxCall(generatedUrl, functionToCall = function(data) {
            parseSelect(selectId, data.contenido);
        }, 'GET', null);
    };
}

function urlGenerator(selectId, source) {
    var select = document.querySelector(selectId);
    return source + select.options[select.selectedIndex].value;
}

// Traigo la informacion segun la URL usando AJAX
function ajaxCall(url, functionToCall, method, params) {
    var ajaxCall = new XMLHttpRequest();

    ajaxCall.onreadystatechange = function() {
        if (ajaxCall.readyState == 4 && ajaxCall.status == 200) {
            try {
                var respuesta = JSON.parse(ajaxCall.responseText);
                functionToCall(respuesta);
            } catch (e) {
                functionToCall(ajaxCall.responseText);
            }
        }
    };

    if (method.toUpperCase() === 'GET' || method.toUpperCase() === 'POST') {
        ajaxCall.open(method, url, true);
    }

    if (method.toUpperCase() === 'POST') {
        ajaxCall.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    }

    if (method === 'GET') {
        ajaxCall.send();
    } else if (method.toUpperCase() === 'POST' && (typeof params === 'object' || typeof params === 'undefined')) {
        ajaxCall.send(serialize(params));
    }
}

/*
 *  Esta funcion se utiliza para convertir el objeto enviado
 *  por POST en un formato aceptable para el sitio destinatario,
 *  en este caso resulta ser registrarPersona
 */
serialize = function(obj) {
    var str = [];
    for (var p in obj)
        if (obj.hasOwnProperty(p)) {
            str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
        }
    return str.join("&");
};

// Esta funcion rellena el select de la informacion traida
function parseSelect(selectId, ajaxData) {
    var select = document.querySelector(selectId);

    // Borro lo que antes habia en el select para luego ponerle los datos nuevos.
    select.innerHTML = '<option></option value="">';

    /*
     * Recorro cada indice de ajaxData y creo
     * las options para meterlas en el select.
     */
    for (var index in ajaxData) {
        option = document.createElement('option');
        option.value = ajaxData[index];
        option.innerHTML = index;
        select.appendChild(option);
    }
}
