<h1>Documentacion:</h1>

  <p>El siguiente programa hace dos llamadas AJAX, una por POST y una por GET.</p>
  
  <b>La función que ejecuta la llamada AJAX es: ajaxCall()</b>
  
  ajaxCall() recibe cuatro argumentos:
  <ul>
  <li><b>URL</b>
      <p>
        Es la direccion a donde enviamos o desde donde recibimos los datos.<br>
        En el ejemplo la dirección corresponde a la de un archivo JSON.
      <p>
      </li>
  
  
  <li><b>functionToCall()</b>
      <p>
        Esta funcion que le pasamos como argumento al ajaxCall() la utilizamos para hacer algo con la información que enviamos o recibimos.<br>
        En nuestro ejemplo usamos parseSelect() para traer los datos por GET.<br>
        En el ejemplo de POST pasamos como argumento una funcion anonima que recibia la respuesta del servidor por
        parametro y la mostraba por consola con un console.log();
      <p>
  </li>
  
  
  <li><b>method</b>
    <p>
      Solo se aceptan dos posibles valores de method, 'POST' o 'GET'.
    </p>
  </li>


  <li><b>params</b>
    <p>
      En nuestro caso es el usuario que registramos por POST, una variable de tipo objeto.
    </p>
  </li>
  <ul>
